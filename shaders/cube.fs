#version 330

uniform float TIME;

in  vec3 vsoNormal;
in  vec3 vsoModPos;
in  vec2 vsoTexCoord;
out vec4 fragColor;

const float MIN = 0.05;
const float MAX = 1.0 - MIN;

void main(void) {
  vec2 p = vsoTexCoord - vec2(0.5, 0.5);
  float alpha = 0.0;
  /*if (vsoTexCoord.x < MIN || vsoTexCoord.x > MAX || vsoTexCoord.y < MIN || vsoTexCoord.y > MAX) {
    alpha = 0.25;
  } */

  float d = (p.x * p.x + p.y * p.y) * 0.75 + 0.25 * (sin(vsoTexCoord.y + TIME * 0.17894) + cos(vsoTexCoord.x + TIME * 0.07131));
  float o = 0.5 * (sin(vsoTexCoord.y - TIME * 0.05894) + cos(vsoTexCoord.x - TIME * 0.156493));
  fragColor = vec4(1.0 - o, 1.0 - o, 1.0, d * d * 4.0);
}
