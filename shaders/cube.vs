#version 330

uniform float TIME;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
layout (location = 0) in vec3 vsiPosition;
layout (location = 1) in vec3 vsiNormal;
layout (location = 2) in vec2 vsiTexCoord;
 
out vec2 vsoTexCoord;

void main(void) {
  float offset = cos(vsiPosition.x - vsiPosition.y + TIME * 0.05) * 0.1;
  vsoTexCoord = vec2(vsiTexCoord.x + offset, 1.0 - vsiTexCoord.y + offset);
  vec3 offset_vec = vec3(offset, offset, offset);

  vec4 mp = modelViewMatrix * vec4(vsiPosition + offset_vec, 1.0);
  gl_Position = projectionMatrix * mp;
}
