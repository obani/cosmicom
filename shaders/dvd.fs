#version 330

uniform sampler2D tex;
uniform float x;
uniform float y;
uniform float offset;

in  vec3 vsoNormal;
in  vec3 vsoModPos;
in  vec2 vsoTexCoord;
out vec4 fragColor;

float c(float f) { return cos(f + offset) * 0.5 + 0.5; }
float s(float f) { return sin(f + offset) * 0.5 + 0.5; }

void main(void) {
  if (vsoTexCoord.x > 0.0 || vsoTexCoord.y > 0.0) {
    fragColor = vec4(0.0);
  } else {
    fragColor = vec4(texture(tex, vec2(vsoTexCoord.x, -vsoTexCoord.y)).rgb
                  *
                  normalize(vec3(c(x), c(y), s(x + y))),
                  1.0);
  }
}
