#version 330

uniform float x;
uniform float y;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
layout (location = 0) in vec3 vsiPosition;
layout (location = 1) in vec3 vsiNormal;
layout (location = 2) in vec2 vsiTexCoord;
 
out vec2 vsoTexCoord;

void main(void) {
  vec2 p = vsiPosition.xy;
  vsoTexCoord = p;
  gl_Position = vec4(p.x + x, p.y + y, vsiPosition.z, 1.0);
}
