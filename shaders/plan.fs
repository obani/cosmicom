#version 330

uniform float TIME;

in  vec3 vsoNormal;
in  vec3 vsoModPos;
in  vec2 vsoTexCoord;
out vec4 fragColor;

const float MIN = 0.05;
const float MAX = 1.0 - MIN;
const int STEPS = 4;


void main(void) {
  vec2 p = vsoTexCoord - vec2(0.5, 0.5);
  vec3 c = vec3(0.0);
  for(int i = 0; i < STEPS; ++i) {
    float t = TIME + float(i * 46365363);
    float step = cos(t * 0.51531);

    float d = step * distance(p, vec2(0.0, 0.0)) + (0.5 - step);
    c += vec3(cos(t * 0.12415), sin(t * 0.1413643) * 0.5, cos(t * 0.001531) * 0.25) * d;
  }
  fragColor = vec4(c, 1.0);
}
