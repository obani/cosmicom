#version 330

uniform float TIME;

uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
layout (location = 0) in vec3 vsiPosition;
layout (location = 1) in vec3 vsiNormal;
layout (location = 2) in vec2 vsiTexCoord;
 
out vec2 vsoTexCoord;

void main(void) {
  vsoTexCoord = (vsiPosition.xy + vec2(1.0)) * 0.5;
  gl_Position = vec4(vsiPosition, 1.0);
}
