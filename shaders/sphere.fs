#version 330

uniform float TIME;

in  vec3 vsoNormal;
in  vec3 vsoModPos;
in  vec2 vsoTexCoord;
out vec4 fragColor;

void main(void) {
  float alpha = 1.0;
  if (int((vsoTexCoord.x + vsoTexCoord.y) * 200.0 + TIME * 5.0) % 2 == 0) {
    alpha = 0.0;
  }


  fragColor = vec4(vsoTexCoord.x, 0.0, vsoTexCoord.y, alpha);
}
