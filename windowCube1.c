/*!\file window.c
 *
 * \brief Lumière positionnelle + Phong + Bump mapping + Normal
 * mapping + textures et geometry shader
 *
 * \author Farès BELHADJ, amsi@ai.univ-paris8.fr
 * \date March 19 2018
 */
#include <assert.h>
#include <GL4D/gl4du.h>
#include <GL4D/gl4duw_SDL2.h>
#include <SDL_image.h>


static int WIDTH = 640, HEIGHT = 480;
static GLuint pid1 = 0;
static GLuint cube = 0;

static void init() {
  glClearColor(0.0f, 0.0f, 0.1f, 0.0f);

  pid1 = gl4duCreateProgram("<vs>shaders/cube.vs", "<fs>shaders/cube.fs", NULL);
  gl4duGenMatrix(GL_FLOAT, "modelViewMatrix");
  gl4duGenMatrix(GL_FLOAT, "projectionMatrix");

  glViewport(0, 0, WIDTH, HEIGHT);
  gl4duBindMatrix("projectionMatrix");
  gl4duLoadIdentityf();
  gl4duFrustumf(-0.5, 0.5, -0.5 * HEIGHT / WIDTH, 0.5 * HEIGHT / WIDTH, 1.0, 1000.0);
  gl4duBindMatrix("modelViewMatrix");

  cube = gl4dgGenCubef();
}

static void drawCube(GLfloat time) {
  static GLfloat a0 = 0.0;
  static GLfloat t0 = 0.0;
  
  gl4duTranslatef(0, 0, -3);

  glUseProgram(pid1);
  gl4duRotatef(-a0, 1, 1, 0);
  glUniform1f(glGetUniformLocation(pid1, "TIME"), time);
  gl4duSendMatrices();
  gl4dgDraw(cube);

  GLfloat dt = (time - t0);
  t0 = time;

  a0 += dt * 2.0;
}

static void draw() {
  GLfloat time = SDL_GetTicks() / 1000.0;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  gl4duBindMatrix("modelViewMatrix");
  gl4duLoadIdentityf();

  drawCube(time);
}



static void quit() { gl4duClean(GL4DU_ALL); }

int main(int argc, char ** argv) {
  if(!gl4duwCreateWindow(argc, argv, "GL4Dummies", 10, 10, WIDTH, HEIGHT, GL4DW_SHOWN))
    return 1;

  init();
  atexit(quit);
  gl4duwDisplayFunc(draw);
  gl4duwMainLoop();
  return 0;
}
