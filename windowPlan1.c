/*!\file window.c
 *
 * \brief Lumière positionnelle + Phong + Bump mapping + Normal
 * mapping + textures et geometry shader
 *
 * \author Farès BELHADJ, amsi@ai.univ-paris8.fr
 * \date March 19 2018
 */
#include <assert.h>
#include <stdlib.h>
#include <GL4D/gl4du.h>
#include <GL4D/gl4duw_SDL2.h>
#include <SDL_image.h>

static int WIDTH = 640, HEIGHT = 480;
static GLuint pid1 = 0;
static GLuint plan = 0;

static void init() {
  glClearColor(0.0f, 0.0f, 0.1f, 0.0f);

  pid1 = gl4duCreateProgram("<vs>shaders/plan.vs", "<fs>shaders/plan.fs", NULL);
  gl4duGenMatrix(GL_FLOAT, "modelViewMatrix");
  gl4duGenMatrix(GL_FLOAT, "projectionMatrix");

  glViewport(0, 0, WIDTH, HEIGHT);
  gl4duBindMatrix("projectionMatrix");
  gl4duLoadIdentityf();
  gl4duFrustumf(-0.5, 0.5, -0.5 * HEIGHT / WIDTH, 0.5 * HEIGHT / WIDTH, 1.0, 1000.0);
  gl4duBindMatrix("modelViewMatrix");

  plan = gl4dgGenQuadf();
}


static void drawPlan(GLfloat time) {
  static GLfloat dt = 0.f;
  const float R = 1.f/(float)RAND_MAX;
  const int N = 21;
  const float NM = N - 1;

  if((rand() % (N + (int)(cos(time * 0.05f) * NM))) == 0)
    dt += (rand() * R) * 0.125f - 0.0675f; 

  glUseProgram(pid1);
  glUniform1f(glGetUniformLocation(pid1, "TIME"), dt + time);
  gl4duSendMatrices();
  gl4dgDraw(plan);
}

static void draw() {
  GLfloat time = SDL_GetTicks() / 2000.0;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  gl4duBindMatrix("modelViewMatrix");
  gl4duLoadIdentityf();

  drawPlan(time);
}



static void quit() { gl4duClean(GL4DU_ALL); }

int main(int argc, char ** argv) {
  if(!gl4duwCreateWindow(argc, argv, "GL4Dummies", 10, 10, WIDTH, HEIGHT, GL4DW_SHOWN))
    return 1;

  init();
  atexit(quit);
  gl4duwDisplayFunc(draw);
  gl4duwMainLoop();
  return 0;
}
