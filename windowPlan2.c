/*!\file window.c
 *
 * \brief Lumière positionnelle + Phong + Bump mapping + Normal
 * mapping + textures et geometry shader
 *
 * \author Farès BELHADJ, amsi@ai.univ-paris8.fr
 * \date March 19 2018
 */
#include <assert.h>
#include <stdlib.h>
#include <GL4D/gl4du.h>
#include <GL4D/gl4duw_SDL2.h>
#include <SDL_image.h>

#include "imageLoader.h"

static int WIDTH = 640, HEIGHT = 480;
static GLuint pid1 = 0;
static GLuint plan = 0;

static GLuint tex[1] = {};

static void init() {
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  pid1 = gl4duCreateProgram("<vs>shaders/dvd.vs", "<fs>shaders/dvd.fs", NULL);
  gl4duGenMatrix(GL_FLOAT, "modelViewMatrix");
  gl4duGenMatrix(GL_FLOAT, "projectionMatrix");

  glViewport(0, 0, WIDTH, HEIGHT);
  gl4duBindMatrix("projectionMatrix");
  gl4duLoadIdentityf();
  gl4duFrustumf(-0.5, 0.5, -0.5 * HEIGHT / WIDTH, 0.5 * HEIGHT / WIDTH, 1.0, 1000.0);
  gl4duBindMatrix("modelViewMatrix");

  glGenTextures(sizeof tex / sizeof *tex, tex);
  loadTexture(tex[0], "img/cosmico.png");
  glEnable(GL_TEXTURE_2D);

  plan = gl4dgGenQuadf();
}

static void draw() {
  const float speed = 0.006f;
  static float speedX = speed * 0.75f, speedY = speed;
  static GLfloat offset = 0.f;
  static GLfloat x = 0.f, y = 0.f;

  const float OFFSETX = 0.08f;
  const float OFFSETY = OFFSETX * 4.f;
  const float LIMIT_LOWX = 0.f - OFFSETX;
  const float LIMIT_HIGHX = 1.f + OFFSETX;
  const float LIMIT_LOWY = 0.f - OFFSETY;
  const float LIMIT_HIGHY = 1.f + OFFSETY;

  if ((x + speedX) >= LIMIT_HIGHX || (x + speedX) <= LIMIT_LOWX) {
    speedX = -speedX;
    offset = (offset + 1.f) - (int)(offset * 0.02) * 50;
  }

  if ((y + speedY) >= LIMIT_HIGHY || (y + speedY) <= LIMIT_LOWY) {
    speedY = -speedY;
    offset = (offset + 1.f) - (int)(offset * 0.02) * 50;
  }

  x += speedX;
  y += speedY;

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_BLEND);
  glEnable(GL_DEPTH_TEST);
  gl4duBindMatrix("modelViewMatrix");
  gl4duLoadIdentityf();

  glUseProgram(pid1);
  
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, tex[0]);
  glUniform1i(glGetUniformLocation(pid1, "tex"), 0);
  glUniform1f(glGetUniformLocation(pid1, "x"), x);
  glUniform1f(glGetUniformLocation(pid1, "y"), y);
  glUniform1f(glGetUniformLocation(pid1, "offset"), offset);

  gl4duSendMatrices();
  gl4dgDraw(plan);
}



static void quit() { gl4duClean(GL4DU_ALL); }

int main(int argc, char ** argv) {
  if(!gl4duwCreateWindow(argc, argv, "GL4Dummies", 10, 10, WIDTH, HEIGHT, GL4DW_SHOWN))
    return 1;

  init();
  atexit(quit);
  gl4duwDisplayFunc(draw);
  gl4duwMainLoop();
  return 0;
}
